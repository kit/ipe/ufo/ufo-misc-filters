/*
 * Copyright (C) 2011-2015 Karlsruhe Institute of Technology
 *
 * This file is part of Ufo.
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <pcilib.h>
#include <pcilib/dma.h>
#include <pcilib/error.h>
#include "ufo-pcilib-task.h"


struct _UfoPcilibTaskPrivate {
    gchar               *device;
    pcilib_t            *pci;
    pcilib_dma_engine_t  dma;
    gboolean             generated;
    guint                number;
    guint                width;
    guint                height;
    gsize                size;      /* the size of undecoded data */
};

static void ufo_task_interface_init (UfoTaskIface *iface);

G_DEFINE_TYPE_WITH_CODE (UfoPcilibTask, ufo_pcilib_task, UFO_TYPE_TASK_NODE,
                         G_IMPLEMENT_INTERFACE (UFO_TYPE_TASK,
                                                ufo_task_interface_init))

#define UFO_PCILIB_TASK_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE((obj), UFO_TYPE_PCILIB_TASK, UfoPcilibTaskPrivate))

enum {
    PROP_0,
    PROP_DEVICE,
    PROP_NUMBER,
    PROP_WIDTH,
    PROP_HEIGHT,
    N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES] = { NULL, };


UfoNode *
ufo_pcilib_task_new (void)
{
    return UFO_NODE (g_object_new (UFO_TYPE_PCILIB_TASK, NULL));
}

static guint32
read_register (UfoPcilibTaskPrivate *priv, uintptr_t addr)
{
    guint32 value;

    pcilib_read (priv->pci, PCILIB_BAR0, addr, sizeof (guint32), &value);
    return value;
}

static void
write_register (UfoPcilibTaskPrivate *priv, uintptr_t addr, guint32 value)
{
    if (pcilib_write (priv->pci, PCILIB_BAR0, addr, sizeof (guint32), &value) != PCILIB_ERROR_SUCCESS)
        g_warning ("Could not write `%x' into %p.", value, (gpointer) addr);

    g_usleep (1000);
}

static void
pci_pcilib_log_message (gpointer arg, const gchar *file, gint line, pcilib_log_priority_t priority, const gchar *message, va_list va)
{
    g_debug ("pcilib: %s [%s:%i]", message, file, line);
}

static void
ufo_pcilib_task_setup (UfoTask *task,
                       UfoResources *resources,
                       GError **error)
{
    UfoPcilibTaskPrivate *priv;
    const pcilib_dma_description_t *dma_description;

    pcilib_set_logger (PCILIB_LOG_DEBUG, pci_pcilib_log_message, NULL);

    priv = UFO_PCILIB_TASK_GET_PRIVATE (task);
    priv->pci = pcilib_open (priv->device, "ipedma");

    if (priv->pci == NULL) {
        g_set_error (error, UFO_TASK_ERROR, UFO_TASK_ERROR_SETUP,
                     "Could not initialize pcilib.");
        return;
    }

    write_register (priv, 0x9000, 0x0);
    write_register (priv, 0x9040, 0xf);
    write_register (priv, 0x9160, 0x0);
    write_register (priv, 0x9164, 0x0);
    write_register (priv, 0x9168, priv->height);
    write_register (priv, 0x9170, priv->number);
    write_register (priv, 0x9180, 0x0);
    write_register (priv, 0x9040, 0xfff000);

    dma_description = pcilib_get_dma_description (priv->pci);

    /* addr_bits == 0 as a conditional for end of list is nowhere documented ... */
    for (pcilib_dma_engine_t dma = 0; dma_description->engines[dma].addr_bits; dma++) {
        const pcilib_dma_engine_description_t *description = &dma_description->engines[dma];

        if (description->direction == PCILIB_DMA_FROM_DEVICE) {
            g_debug ("pcilib: found DMA `%s'", description->name);
            priv->dma = dma;
            break;
        }
    }

    if (pcilib_start_dma (priv->pci, priv->dma, PCILIB_DMA_FLAGS_DEFAULT)) {
        g_set_error (error, UFO_TASK_ERROR, UFO_TASK_ERROR_SETUP, "Could not start DMA");
        return;
    }

    priv->generated = FALSE;
}

static void
ufo_pcilib_task_get_requisition (UfoTask *task,
                                 UfoBuffer **inputs,
                                 UfoRequisition *requisition,
                                 GError **error)
{
    UfoPcilibTaskPrivate *priv;

    priv = UFO_PCILIB_TASK_GET_PRIVATE (task);

    requisition->n_dims = 2;
    requisition->dims[0] = priv->width;
    requisition->dims[1] = priv->height * priv->number;
}

static guint
ufo_pcilib_task_get_num_inputs (UfoTask *task)
{
    return 0;
}

static guint
ufo_pcilib_task_get_num_dimensions (UfoTask *task,
                                    guint input)
{
    return 0;
}

static UfoTaskMode
ufo_pcilib_task_get_mode (UfoTask *task)
{
    return UFO_TASK_MODE_GENERATOR | UFO_TASK_MODE_CPU;
}

static void
cleanup_pcilib (UfoPcilibTaskPrivate *priv)
{
    gpointer array;

    array = g_malloc (4096);

    /* read stale data */
    while (!pcilib_read_dma_custom (priv->pci, priv->dma, 0, 4096, PCILIB_DMA_FLAG_MULTIPACKET, PCILIB_TIMEOUT_IMMEDIATE, array, NULL))
        ;

    if (pcilib_stop_dma (priv->pci, 0, PCILIB_DMA_FLAGS_DEFAULT))
        g_warning ("Could not stop data transfer");

    if (priv->pci != NULL) {
        pcilib_close (priv->pci);
        priv->pci = NULL;
    }

    g_free (array);
}

static gboolean
ufo_pcilib_task_generate (UfoTask *task,
                          UfoBuffer *output,
                          UfoRequisition *requisition)
{
    UfoPcilibTaskPrivate *priv;
    gsize size;
    gpointer array;
    pcilib_dma_flags_t flags;

    priv = UFO_PCILIB_TASK_GET_PRIVATE (task);

    if (priv->generated) {
        cleanup_pcilib (priv);
        return FALSE;
    }

    size = priv->number * (64 + priv->width * priv->height * 2 + priv->width * priv->height / 320);
    array = ufo_buffer_get_host_array (output, NULL);
    flags = PCILIB_DMA_FLAG_WAIT | PCILIB_DMA_FLAG_MULTIPACKET;

    pcilib_read_dma_custom (priv->pci, priv->dma, 0, size, flags, PCILIB_TIMEOUT_INFINITE, array, NULL);

    priv->generated = TRUE;
    return TRUE;
}

static void
ufo_pcilib_task_set_property (GObject *object,
                              guint property_id,
                              const GValue *value,
                              GParamSpec *pspec)
{
    UfoPcilibTaskPrivate *priv = UFO_PCILIB_TASK_GET_PRIVATE (object);

    switch (property_id) {
        case PROP_DEVICE:
            g_free (priv->device);
            priv->device = g_strdup (g_value_get_string (value));
            break;
        case PROP_NUMBER:
            priv->number = g_value_get_uint (value);
            break;
        case PROP_WIDTH:
            priv->number = g_value_get_uint (value);
            break;
        case PROP_HEIGHT:
            priv->number = g_value_get_uint (value);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
            break;
    }
}

static void
ufo_pcilib_task_get_property (GObject *object,
                              guint property_id,
                              GValue *value,
                              GParamSpec *pspec)
{
    UfoPcilibTaskPrivate *priv = UFO_PCILIB_TASK_GET_PRIVATE (object);

    switch (property_id) {
        case PROP_DEVICE:
            g_value_set_string (value, priv->device);
            break;
        case PROP_NUMBER:
            g_value_set_uint (value, priv->number);
            break;
        case PROP_WIDTH:
            g_value_set_uint (value, priv->width);
            break;
        case PROP_HEIGHT:
            g_value_set_uint (value, priv->height);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
            break;
    }
}

static void
ufo_task_interface_init (UfoTaskIface *iface)
{
    iface->setup = ufo_pcilib_task_setup;
    iface->get_num_inputs = ufo_pcilib_task_get_num_inputs;
    iface->get_num_dimensions = ufo_pcilib_task_get_num_dimensions;
    iface->get_mode = ufo_pcilib_task_get_mode;
    iface->get_requisition = ufo_pcilib_task_get_requisition;
    iface->generate = ufo_pcilib_task_generate;
}

static void
ufo_pcilib_task_class_init (UfoPcilibTaskClass *klass)
{
    GObjectClass *oclass = G_OBJECT_CLASS (klass);

    oclass->set_property = ufo_pcilib_task_set_property;
    oclass->get_property = ufo_pcilib_task_get_property;

    properties[PROP_DEVICE] =
        g_param_spec_string ("device",
                             "Device name",
                             "Device name",
                             "/dev/fpga0",
                             G_PARAM_READWRITE);

    properties[PROP_NUMBER] =
        g_param_spec_uint ("number",
                           "Number of frames to read",
                           "Number of frames to read",
                           1, G_MAXUINT, 1,
                           G_PARAM_READWRITE);

    properties[PROP_WIDTH] =
        g_param_spec_uint ("width",
                           "Width of a frame",
                           "Width of a frame [default: 5120]",
                           1, G_MAXUINT, 5120,
                           G_PARAM_READWRITE);

    properties[PROP_HEIGHT] =
        g_param_spec_uint ("height",
                           "Height of a frame",
                           "Height of a frame [default: 3840]",
                           1, G_MAXUINT, 3840,
                           G_PARAM_READWRITE);

    for (guint i = PROP_0 + 1; i < N_PROPERTIES; i++)
        g_object_class_install_property (oclass, i, properties[i]);

    g_type_class_add_private (oclass, sizeof(UfoPcilibTaskPrivate));
}

static void
ufo_pcilib_task_init(UfoPcilibTask *self)
{
    self->priv = UFO_PCILIB_TASK_GET_PRIVATE(self);
    self->priv->device = g_strdup ("/dev/fpga0");
    self->priv->pci = NULL;
    self->priv->number = 1;
    self->priv->width = 5120;
    self->priv->height = 3840;
}
