kernel void
decode (global unsigned int *input, global float *output, unsigned width, unsigned frame)
{
    unsigned d1;
    unsigned d2;

    /* skip the frame header (+8), read every 8 words and account for that
     * stupid skip every 640 pixels ... */
    size_t base = 8 * (get_global_id (0) + (get_global_id (0) / 640)) + 8;

    base += frame * (8 * get_global_size (0) + 8 * get_global_size (0) / 640 + 16);

    /* index = row_number * 5120 + pixel_number */
    size_t index = (input[base] & 0xfff) * width + ((input[base + 1] >> 16) & 0xfff);

    base += 2;

    d1 = input[base];
    output[index + 0 * 640] = (d1 >> 20);
    output[index + 1 * 640] = (d1 >> 8) & 0xfff;

    d2 = input[base + 1];
    output[index + 2 * 640] = ((d1 << 4) & 0xfff) | (d2 >> 28);
    output[index + 3 * 640] = (d2 >> 16) & 0xfff;
    output[index + 4 * 640] = (d2 >> 4) & 0xfff;

    d1 = input[base + 2];
    output[index + 5 * 640] = ((d2 << 8) & 0xfff) | (d1 >> 24);
    output[index + 6 * 640] = (d1 >> 12) & 0xfff;
    output[index + 7 * 640] = d1 & 0xfff;

    index += width;

    d1 = input[base + 3];
    output[index + 0 * 640] = (d1 >> 20);
    output[index + 1 * 640] = (d1 >> 8) & 0xfff;

    d2 = input[base + 4];
    output[index + 2 * 640] = ((d1 << 4) & 0xfff) | (d2 >> 28);
    output[index + 3 * 640] = (d2 >> 16) & 0xfff;
    output[index + 4 * 640] = (d2 >> 4) & 0xfff;

    d1 = input[base + 5];
    output[index + 5 * 640] = ((d2 << 8) & 0xfff) | (d1 >> 24);
    output[index + 6 * 640] = (d1 >> 12) & 0xfff;
    output[index + 7 * 640] = (d1 & 0xfff);
}
