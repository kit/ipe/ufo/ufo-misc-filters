/*
 * Copyright (C) 2011-2015 Karlsruhe Institute of Technology
 *
 * This file is part of Ufo.
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#include "ufo-decode-task.h"


struct _UfoDecodeTaskPrivate {
    cl_kernel kernel;
    cl_mem input;
    cl_context context;
    guint number;
    guint current;
    gboolean store;
};

static void ufo_task_interface_init (UfoTaskIface *iface);

G_DEFINE_TYPE_WITH_CODE (UfoDecodeTask, ufo_decode_task, UFO_TYPE_TASK_NODE,
                         G_IMPLEMENT_INTERFACE (UFO_TYPE_TASK,
                                                ufo_task_interface_init))

#define UFO_DECODE_TASK_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE((obj), UFO_TYPE_DECODE_TASK, UfoDecodeTaskPrivate))

enum {
    PROP_0,
    PROP_NUMBER,
    PROP_STORE,
    N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES] = { NULL, };

UfoNode *
ufo_decode_task_new (void)
{
    return UFO_NODE (g_object_new (UFO_TYPE_DECODE_TASK, NULL));
}

static void
ufo_decode_task_setup (UfoTask *task,
                       UfoResources *resources,
                       GError **error)
{
    UfoDecodeTaskPrivate *priv;

    priv = UFO_DECODE_TASK_GET_PRIVATE (task);
    priv->kernel = ufo_resources_get_kernel (resources, "decode.cl", "decode", NULL, error);
    priv->context = ufo_resources_get_context (resources);
}

static void
ufo_decode_task_get_requisition (UfoTask *task,
                                 UfoBuffer **inputs,
                                 UfoRequisition *requisition,
                                 GError **error)
{
    UfoDecodeTaskPrivate *priv;

    priv = UFO_DECODE_TASK_GET_PRIVATE (task);
    ufo_buffer_get_requisition (inputs[0], requisition);
    requisition->dims[1] /= priv->number;
}

static guint
ufo_decode_task_get_num_inputs (UfoTask *task)
{
    return 1;
}

static guint
ufo_decode_task_get_num_dimensions (UfoTask *task,
                                    guint input)
{
    return 2;
}

static UfoTaskMode
ufo_decode_task_get_mode (UfoTask *task)
{
    return UFO_TASK_MODE_REDUCTOR | UFO_TASK_MODE_GPU;
}

static gboolean
ufo_decode_task_process (UfoTask *task,
                         UfoBuffer **inputs,
                         UfoBuffer *output,
                         UfoRequisition *requisition)
{
    UfoDecodeTaskPrivate *priv;
    UfoGpuNode *node;
    cl_command_queue cmd_queue;

    priv = UFO_DECODE_TASK_GET_PRIVATE (task);
    node = UFO_GPU_NODE (ufo_task_node_get_proc_node (UFO_TASK_NODE (task)));
    cmd_queue = ufo_gpu_node_get_cmd_queue (node);
    priv->current = 0;

    if (priv->store) {
        cl_mem in_mem;
        cl_event event;
        cl_int errcode;
        gsize size;

        in_mem = ufo_buffer_get_device_array (inputs[0], cmd_queue);
        size = ufo_buffer_get_size (inputs[0]);
        priv->input = clCreateBuffer (priv->context, CL_MEM_READ_ONLY, size, NULL, &errcode);
        UFO_RESOURCES_CHECK_CLERR (errcode);

        UFO_RESOURCES_CHECK_CLERR (clEnqueueCopyBuffer (cmd_queue, in_mem, priv->input, 0, 0, size, 0, NULL, &event));
        UFO_RESOURCES_CHECK_CLERR (clWaitForEvents (1, &event));
    }
    else {
        priv->input = ufo_buffer_get_device_array (inputs[0], cmd_queue);
    }

    return FALSE;
}

static gboolean
ufo_decode_task_generate (UfoTask *task,
                          UfoBuffer *output,
                          UfoRequisition *requisition)
{
    UfoDecodeTaskPrivate *priv;
    UfoGpuNode *node;
    UfoProfiler *profiler;
    cl_command_queue cmd_queue;
    cl_mem out_mem;
    size_t work_size;
    size_t local_work_size = 64;

    priv = UFO_DECODE_TASK_GET_PRIVATE (task);

    if (priv->current == priv->number) {
        if (priv->store) {
            UFO_RESOURCES_CHECK_CLERR (clReleaseMemObject (priv->input));
            priv->input = NULL;
        }

        return FALSE;
    }

    profiler = ufo_task_node_get_profiler (UFO_TASK_NODE (task));
    node = UFO_GPU_NODE (ufo_task_node_get_proc_node (UFO_TASK_NODE (task)));
    cmd_queue = ufo_gpu_node_get_cmd_queue (node);
    out_mem = ufo_buffer_get_device_array (output, cmd_queue);

    UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (priv->kernel, 0, sizeof (cl_mem), &priv->input));
    UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (priv->kernel, 1, sizeof (cl_mem), &out_mem));
    UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (priv->kernel, 2, sizeof (unsigned), &requisition->dims[0]));
    UFO_RESOURCES_CHECK_CLERR (clSetKernelArg (priv->kernel, 3, sizeof (unsigned), &priv->current));

    work_size = requisition->dims[0] * requisition->dims[1] / 16;
    ufo_profiler_call (profiler, cmd_queue, priv->kernel, 1, &work_size, &local_work_size);

    priv->current++;
    return TRUE;
}

static void
ufo_decode_task_set_property (GObject *object,
                              guint property_id,
                              const GValue *value,
                              GParamSpec *pspec)
{
    UfoDecodeTaskPrivate *priv;

    priv = UFO_DECODE_TASK_GET_PRIVATE (object);

    switch (property_id) {
        case PROP_NUMBER:
            priv->number = g_value_get_uint (value);
            break;
        case PROP_STORE:
            priv->store = g_value_get_boolean (value);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
            break;
    }
}

static void
ufo_decode_task_get_property (GObject *object,
                              guint property_id,
                              GValue *value,
                              GParamSpec *pspec)
{
    UfoDecodeTaskPrivate *priv;

    priv = UFO_DECODE_TASK_GET_PRIVATE (object);

    switch (property_id) {
        case PROP_NUMBER:
            g_value_set_uint (value, priv->number);
            break;
        case PROP_STORE:
            g_value_set_boolean (value, priv->store);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
            break;
    }
}

static void
ufo_decode_task_finalize (GObject *object)
{
    UfoDecodeTaskPrivate *priv;

    priv = UFO_DECODE_TASK_GET_PRIVATE (object);

    if (priv->store && priv->input != NULL) {
        UFO_RESOURCES_CHECK_CLERR (clReleaseMemObject (priv->input));
        priv->input = NULL;
    }

    G_OBJECT_CLASS (ufo_decode_task_parent_class)->finalize (object);
}

static void
ufo_task_interface_init (UfoTaskIface *iface)
{
    iface->setup = ufo_decode_task_setup;
    iface->get_num_inputs = ufo_decode_task_get_num_inputs;
    iface->get_num_dimensions = ufo_decode_task_get_num_dimensions;
    iface->get_mode = ufo_decode_task_get_mode;
    iface->get_requisition = ufo_decode_task_get_requisition;
    iface->process = ufo_decode_task_process;
    iface->generate = ufo_decode_task_generate;
}

static void
ufo_decode_task_class_init (UfoDecodeTaskClass *klass)
{
    GObjectClass *oclass = G_OBJECT_CLASS (klass);

    oclass->set_property = ufo_decode_task_set_property;
    oclass->get_property = ufo_decode_task_get_property;
    oclass->finalize = ufo_decode_task_finalize;

    properties[PROP_NUMBER] =
        g_param_spec_uint ("number",
            "Number of frames to decode at once",
            "Number of frames to decode at once",
            1, G_MAXUINT, 1,
            G_PARAM_READWRITE);

    properties[PROP_STORE] =
        g_param_spec_boolean ("store",
            "If TRUE store the input for later decoding",
            "If TRUE store the input for later decoding",
            FALSE,
            G_PARAM_READWRITE);

    for (guint i = PROP_0 + 1; i < N_PROPERTIES; i++)
        g_object_class_install_property (oclass, i, properties[i]);

    g_type_class_add_private (oclass, sizeof(UfoDecodeTaskPrivate));
}

static void
ufo_decode_task_init(UfoDecodeTask *self)
{
    self->priv = UFO_DECODE_TASK_GET_PRIVATE(self);
    self->priv->number = 1;
    self->priv->input = NULL;
    self->priv->store = FALSE;
}
