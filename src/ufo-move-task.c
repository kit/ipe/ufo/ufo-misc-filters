/*
 * Copyright (C) 2011-2015 Karlsruhe Institute of Technology
 *
 * This file is part of Ufo.
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#include "ufo-move-task.h"


typedef enum {
    DEST_HOST = 0,
    DEST_DEVICE,
    DEST_IMAGE,
    N_DESTINATION
} Destination;

static const gchar* destinations[] = {
    "host",
    "device",
    "image",
    NULL
};

struct _UfoMoveTaskPrivate {
    Destination destination;
};

static void ufo_task_interface_init (UfoTaskIface *iface);

G_DEFINE_TYPE_WITH_CODE (UfoMoveTask, ufo_move_task, UFO_TYPE_TASK_NODE,
                         G_IMPLEMENT_INTERFACE (UFO_TYPE_TASK,
                                                ufo_task_interface_init))

#define UFO_MOVE_TASK_GET_PRIVATE(obj) (G_TYPE_INSTANCE_GET_PRIVATE((obj), UFO_TYPE_MOVE_TASK, UfoMoveTaskPrivate))

enum {
    PROP_0,
    PROP_DESTINATION,
    N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES] = { NULL, };

UfoNode *
ufo_move_task_new (void)
{
    return UFO_NODE (g_object_new (UFO_TYPE_MOVE_TASK, NULL));
}

static void
ufo_move_task_setup (UfoTask *task,
                     UfoResources *resources,
                     GError **error)
{
}

static void
ufo_move_task_get_requisition (UfoTask *task,
                               UfoBuffer **inputs,
                               UfoRequisition *requisition,
                               GError **error)
{
    ufo_buffer_get_requisition (inputs[0], requisition);
}

static guint
ufo_move_task_get_num_inputs (UfoTask *task)
{
    return 1;
}

static guint
ufo_move_task_get_num_dimensions (UfoTask *task,
                                  guint input)
{
    return 2;
}

static UfoTaskMode
ufo_move_task_get_mode (UfoTask *task)
{
    return UFO_TASK_MODE_PROCESSOR | UFO_TASK_MODE_GPU;
}

static gdouble
get_event_time (cl_event event)
{
    gulong start;
    gulong end;

    UFO_RESOURCES_CHECK_CLERR (clWaitForEvents (1, &event));
    UFO_RESOURCES_CHECK_CLERR (clGetEventProfilingInfo (event, CL_PROFILING_COMMAND_START, sizeof (cl_ulong), &start, NULL));
    UFO_RESOURCES_CHECK_CLERR (clGetEventProfilingInfo (event, CL_PROFILING_COMMAND_END, sizeof (cl_ulong), &end, NULL));
    UFO_RESOURCES_CHECK_CLERR (clReleaseEvent (event));
    return (end - start) / 1000.0 / 1000.0;
}

static gboolean
ufo_move_task_process (UfoTask *task,
                       UfoBuffer **inputs,
                       UfoBuffer *output,
                       UfoRequisition *requisition)
{
    UfoMoveTaskPrivate *priv;
    cl_command_queue queue;
    cl_mem in_mem;
    cl_mem out_mem;
    cl_event event;
    gfloat *out_array;
    gsize size;
    gdouble time_ms;
    size_t origin[3] = {0, 0, 0};
    size_t region[3] = {0, 0, 1};

    priv = UFO_MOVE_TASK_GET_PRIVATE (task);
    queue = ufo_gpu_node_get_cmd_queue (UFO_GPU_NODE (ufo_task_node_get_proc_node (UFO_TASK_NODE (task))));
    in_mem = ufo_buffer_get_device_array (inputs[0], queue);
    size = ufo_buffer_get_size (inputs[0]);

    switch (priv->destination) {
        case DEST_IMAGE:
            out_mem = ufo_buffer_get_device_image (output, queue);
            region[0] = requisition->dims[0];
            region[1] = requisition->dims[1];

            UFO_RESOURCES_CHECK_CLERR (clEnqueueCopyBufferToImage (queue,
                                                                   in_mem, out_mem,
                                                                   0, origin, region,
                                                                   0, NULL, &event));

            time_ms = get_event_time (event);
            g_print ("clEnqueueCopyBufferToImage  time=%3.5f ms  bandwidth=%3.5f MB/s\n",
                      time_ms, size / (time_ms / 1000.0) / 1024 / 1024);
            break;

        case DEST_DEVICE:
            out_mem = ufo_buffer_get_device_array (output, queue);
            UFO_RESOURCES_CHECK_CLERR (clEnqueueCopyBuffer (queue,
                                                            in_mem, out_mem,
                                                            0, 0, size,
                                                            0, NULL, &event));
            time_ms = get_event_time (event);
            g_print ("clEnqueueCopyBuffer         time=%3.5f ms  bandwidth=%3.5f MB/s\n",
                      time_ms, size / (time_ms / 1000.0) / 1024 / 1024);
            break;

        case DEST_HOST:
            out_array = ufo_buffer_get_host_array (output, queue);
            UFO_RESOURCES_CHECK_CLERR (clEnqueueReadBuffer (queue,
                                                            in_mem, CL_TRUE,
                                                            0, size, out_array,
                                                            0, NULL, &event));
            time_ms = get_event_time (event);
            g_print ("clEnqueueReadBuffer         time=%3.5f ms  bandwidth=%3.5f MB/s\n",
                      time_ms, size / (time_ms / 1000.0) / 1024 / 1024);
        default:
            break;
    }

    return TRUE;
}

static void
ufo_move_task_set_property (GObject *object,
                            guint property_id,
                            const GValue *value,
                            GParamSpec *pspec)
{
    UfoMoveTaskPrivate *priv = UFO_MOVE_TASK_GET_PRIVATE (object);

    switch (property_id) {
        case PROP_DESTINATION:
            for (guint i = 0; i < N_DESTINATION; i++) {
                if (g_strcmp0 (destinations[i], g_value_get_string (value)) == 0)
                    priv->destination = (Destination) i;
            }
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
            break;
    }
}

static void
ufo_move_task_get_property (GObject *object,
                              guint property_id,
                              GValue *value,
                              GParamSpec *pspec)
{
    UfoMoveTaskPrivate *priv = UFO_MOVE_TASK_GET_PRIVATE (object);

    switch (property_id) {
        case PROP_DESTINATION:
            g_value_set_string (value, destinations[priv->destination]);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
            break;
    }
}

static void
ufo_move_task_finalize (GObject *object)
{
    G_OBJECT_CLASS (ufo_move_task_parent_class)->finalize (object);
}

static void
ufo_task_interface_init (UfoTaskIface *iface)
{
    iface->setup = ufo_move_task_setup;
    iface->get_num_inputs = ufo_move_task_get_num_inputs;
    iface->get_num_dimensions = ufo_move_task_get_num_dimensions;
    iface->get_mode = ufo_move_task_get_mode;
    iface->get_requisition = ufo_move_task_get_requisition;
    iface->process = ufo_move_task_process;
}

static void
ufo_move_task_class_init (UfoMoveTaskClass *klass)
{
    GObjectClass *oclass = G_OBJECT_CLASS (klass);

    oclass->set_property = ufo_move_task_set_property;
    oclass->get_property = ufo_move_task_get_property;
    oclass->finalize = ufo_move_task_finalize;

    properties[PROP_DESTINATION] =
        g_param_spec_string ("destination",
            "Destination: `host', `device' or `image'.",
            "Destination: `host', `device' or `image'.",
            "device",
            G_PARAM_READWRITE);

    for (guint i = PROP_0 + 1; i < N_PROPERTIES; i++)
        g_object_class_install_property (oclass, i, properties[i]);

    g_type_class_add_private (oclass, sizeof(UfoMoveTaskPrivate));
}

static void
ufo_move_task_init(UfoMoveTask *self)
{
    self->priv = UFO_MOVE_TASK_GET_PRIVATE(self);
    self->priv->destination = DEST_DEVICE;
}
