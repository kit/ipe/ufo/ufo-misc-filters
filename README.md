## ufo-misc-filters

Miscellaneous filters either not ripe or too specific for inclusion into the
main filters distribution.


### pcilib

Access camera data via pcilib and without libuca.


### decode

Decodes UFO camera frames on GPU.
